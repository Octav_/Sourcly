var fs = require('fs');
var http = require('http');
var express = require('express');
var request = require('request');
var cheerio = require('cheerio');
var app = express();

var items = [];
var urls = [
	'http://www.phonearena.com/feed/',
	'http://sammyhub.com/feed/',
	'http://global.samsungtomorrow.com/feed/',
	'http://feed.androidauthority.com/',
	'http://www.tizenexperts.com/feed/',
	'http://www.businesskorea.co.kr/rss.xml',
	'http://feeds.feedburner.com/pocketnow',
	'http://www.gsmarena.com/rss-news-reviews.php3',
	'http://www.technobuffalo.com/feed/',
	'http://androidworld.nl/feed/',
	'http://www.gforgames.com/feed/',
	'https://feeds.feedburner.com/androidcentral',
	'https://www.theverge.com/mobile/rss/index.xml',
	'http://www.engadget.com/rss.xml',
	'http://9to5google.com/feed/',
	'http://www.forbes.com/technology/feed/',
	'http://feeds2.feedburner.com/fone-arena',
	'http://www.digitaltrends.com/mobile/feed/',
	'http://www.cnet.com/rss/android-update/',
	'http://feeds.slashgear.com/slashgear'
];

for(var i = 0, len = urls.length; i < len; i++) {
	(function(i) {
		request(urls[i], function(error, response, html) {
			console.log('Requesting ' + urls[i]);
				if(!error) {
					console.log('Got a response.');

	    				var $ = cheerio.load(html.replace(/<!\[CDATA\[([^\]]+)]\]>/ig, "$1"));
					var name = $('image title').text();

					if(name == "")
						name = $('channel title').first().text();
				
	    				$('item').each(function(i, element) {
						itemTitle = $(this).find('title').text();
						itemDate = $(this).find('pubdate').text();
						itemLink = $(this).find('guid').text();

				/*if(name.indexOf('BusinessKorea') > -1)
					itemLink = itemLink.split(" ")[2];*/

					if(name.indexOf('Android Central') > -1)
						name = name.substr(0, 15);

						var item = {
							name: name,
							title: itemTitle,
							date: itemDate,
							link: itemLink
						};

					items.push(item);
					});
	    			}
				else 
					console.error('ERROR: Could not fetch a response for ' + urls[i]);

			fs.writeFile('output.json', JSON.stringify(items, null, 4), function(err){
	    			console.log('File successfully written! - Check your project directory for the output.json file');
			});
		});
	})(i);
}

var server = app.listen('8081')
console.log('Magic happens on port 8081');
server.close()
exports = module.exports = app;
console.log('Number of sources parsed: ' + urls.length.toString());
